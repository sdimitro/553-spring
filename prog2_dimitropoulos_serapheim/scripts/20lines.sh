#!/bin/sh

if [ "$#" -gt 1 ]
then
	(head -n 10 && tail -n 10) < "$1" > "$2"
else
	echo 'usage: 20lines.sh <input file> <output file>'
fi
