#!/bin/sh

# WARNING: This scripts should be run from the scripts/ directory
#          of this repository

INDIR='../../prog2_dimitropoulos_serapheim'
TARFILE='prog2_dimitropoulos_serapheim.tar'
OUTDIR='../..'

tar -zcvf "${OUTDIR}/${TARFILE}" ${INDIR}
