File Map:
|- README.txt             <- this file
|- prog2_report.pdf       <- the report
|- prog2_sourcecode.txt   <- the code of the Shared Memory Sort
|                            as requested for the assignment
|- scripts                <- folder with all the scripts used
|   |- pkg_asgn.sh        <- tar this folder for submission
|   |- src2txt.sh         <- produce prog2_sourcecode.txt from my source
|   |- 20lines.sh         <- takes the first and last 10 lines of
|                            a file and puts them into a new one
| - configs               <- the directory of all the configs that
|                            I had to change so you don't have to
|                            look everywhere for them
|- output                 <- output files are here
|- code                   <- all the code is located there
|   |- my-xsort           <- the root dir of my Shared Memory Sort
|   |    |- src           <- source code of Shared Memory Sort
|   |- hdpsort            <- the root dir of the Hadoop project
|   |    |- src           <- source code of Hadoop Sort
|   |- sprksort           <- the root dir of the Spark project
|   |    |- src           <- source code of Spark Sort
|- screenshots            <- screenshot directory
    |- EC2                <- EC2 screenshots
    |- Spark              <- Spark screenshots
    |- Hadoop             <- Hadoop screenshots
    |- MySort             <- Shared Memory sort screenshots


Commands:
1] Compilation:
   All the code was developed and incrementally build with Maven
   (see prog2_report.pdf for Maven & Java versions). Therefore in
   order to compile a project you need to go to its root directory
   and run `$ mvn package`. For example if you wanted to compile
   my Shared Memory Sort you would do the following:
   `$ cd code/my-xsort; mvn package`

2] Execution (Single-Instance):
   Make sure you have Java 8 (preferably the OpenJDK variant).
   Then just go the root directory of the code that you want
   to run and run com.{xsort,hdpsort,sprksort}.App passing the
   gensort generated file as the first argument and adding any
   jar that exists in the target/ directory in the classpath.
   For example to run my Shared Memory Sort on file called
   test.txt I would do the following:
   `$ cd code/my-xsort
    $ java -cp target/my-xsort-1.0-SNAPSHOT.jar com.xsort.App test.txt`
   ALSO: make sure the code has been compiled before trying
         the above, and for Hadoop/Spark use their own binaries
         instead of java (Dah!). Also Hadoop has an output directory.
          Example:
         '$ hadoop jar target/hdpsort-1.0-SNAPSHOT.jar   \
                       com.hdpsort.app.App               \
                       input/unsorted-text.txt           \
                       [..add any other options here..]  \
                       output'
         '$ spark-submit --class "com.sprksort.app.App"  \
                        [..add any other options here..] \
                        target/sprksort-1.0-SNAPSHOT.jar \
                        unsorted-text.txt'

3] Execution (16-Instances):
   Similar to the above for master but also check
   config files.

4] Some final notes:
   After running my code make sure to run the unix2dos utility
   on the sorted output. Depending on the Linux  distro that
   you are using and your system's defaults & settings newlines
   in files are either '\n' or '\r\n' and valsort expects the
   latter (I think or the other way around, anyway) and it will
   show errors if the file is not formated as it expects.
   Therefore before running valsort on my output file make sure
   you run the unix2dos utility which you can probably get
   from your distribution's repositories. Thank you.

Please let me know if you find any issues!

Serapheim Dimitropoulos
sdimitro@hawk.iit.edu
