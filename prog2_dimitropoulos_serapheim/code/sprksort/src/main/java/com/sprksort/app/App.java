package com.sprksort.app;

import org.apache.spark.api.java.*;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.function.Function;

public class App {
	public final static int DEFAULT_PARTITIONS = 1024;

	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("error: please pass the name of" +
					   " the file to be sorted as an" +
					   " argument!");
			System.exit(0);
		}

		String target = args[0];
		SparkConf conf = new SparkConf().setAppName("SprkSort");
		JavaSparkContext sc = new JavaSparkContext(conf);
		JavaRDD<String> toBeSorted = sc.textFile(target);

		JavaRDD<String> sorted =
			toBeSorted.sortBy(
				new Function<String, String>() {
					@Override
					public String call(String s)
					{ return s; }
				},
				true,
				DEFAULT_PARTITIONS);
		
		
		sorted.saveAsTextFile("output");
		sc.stop();
	}
}
