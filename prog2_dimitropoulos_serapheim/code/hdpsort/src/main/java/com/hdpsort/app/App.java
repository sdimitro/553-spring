package com.hdpsort.app;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.mapred.lib.*;

public class App {
   // Looking at the program closely you can see
   // that I'm basically creating a huge map
   // where each line is the key and points
   // to an empty String. The reason why I am
   // doing that is that I really do not need
   // to do any sorting explicitly since Hadoop
   // will do it by itself in the final document
   // implicitly and in ascending order of keys
   // (lexicographical sorting which is what
   //  we want since I use Text objects).
   // Kinda forcing Hadoop here but at least
   // it does what I want.

   public static class Map extends MapReduceBase
		   implements Mapper<LongWritable, Text, Text, Text> {

     private final static Text empty = new Text("");
     private Text word = new Text();

     public void map(LongWritable key,
		     Text value,
		     OutputCollector<Text, Text> output,
		     Reporter reporter)
	     throws IOException {
       String [] lines = value.toString().split("\n");
       for (String line : lines) { word.set(line); output.collect(word, empty); }
     }
   }

   public static class Reduce extends MapReduceBase
		   implements Reducer<Text, Text, Text, Text> {

     private final static Text empty = new Text("");

     public void reduce(Text key,
		        Iterator<Text> values,
			OutputCollector<Text, Text> output,
			Reporter reporter)
	     throws IOException {
       output.collect(key, new Text(empty));
     }
   }

   public static void main(String[] args) throws Exception {
     // By default send 1024 lines of the file to each
     // Mapper for processing unless told otherwise by
     // the optional third argument passed to the jar
     // file.
     int linesPerMap = 1024;
     if (args.length == 3) linesPerMap = Integer.parseInt(args[2]);

     JobConf conf = new JobConf(App.class);
     conf.setJobName("terasort");

     conf.setOutputKeyClass(Text.class);
     conf.setOutputValueClass(Text.class);

     conf.setMapperClass(Map.class);
     conf.setCombinerClass(Reduce.class);
     conf.setReducerClass(Reduce.class);

     conf.setInputFormat(NLineInputFormat.class);
     conf.setInt("mapreduce.input.lineinputformat.linespermap", linesPerMap);
     conf.setOutputFormat(TextOutputFormat.class);

     FileInputFormat.setInputPaths(conf, new Path(args[0]));
     SequenceFileOutputFormat.setOutputPath(conf, new Path(args[1]));

     JobClient.runJob(conf);
   }
}

